from enum import StrEnum
from itertools import product
from pathlib import Path

data_file = Path(__file__).parent / "2.txt"


class Turn(StrEnum):
    ROCK = "A"
    PAPER = "B"
    SCISSORS = "C"

    def get_turn_value(self) -> int:
        return tuple(type(self)).index(self) + 1


class Plan(StrEnum):
    LOSE = "X"
    DRAW = "Y"
    WIN = "Z"

    def get_plan_value(self) -> int:
        return tuple(type(self)).index(self) * 3


def produce_scores_table() -> dict:
    scores = dict()
    for (opponent, us) in product("ABC", "XYZ"):
        match opponent, us:
            case Turn.ROCK, Plan.LOSE:
                planned_move = Turn.SCISSORS
            case Turn.ROCK, Plan.DRAW:
                planned_move = Turn.ROCK
            case Turn.ROCK, Plan.WIN:
                planned_move = Turn.PAPER
            case Turn.SCISSORS, Plan.LOSE:
                planned_move = Turn.PAPER
            case Turn.SCISSORS, Plan.DRAW:
                planned_move = Turn.SCISSORS
            case Turn.SCISSORS, Plan.WIN:
                planned_move = Turn.ROCK
            case Turn.PAPER, Plan.LOSE:
                planned_move = Turn.ROCK
            case Turn.PAPER, Plan.DRAW:
                planned_move = Turn.PAPER
            case Turn.PAPER, Plan.WIN:
                planned_move = Turn.SCISSORS
        scores[f"{opponent} {us}"] = (
            Plan(us).get_plan_value() + planned_move.get_turn_value()
        )

    return scores


if __name__ == "__main__":
    scores_table = produce_scores_table()
    print(sum(scores_table[line] for line in data_file.read_text().split("\n")))
