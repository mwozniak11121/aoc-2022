import re
from dataclasses import dataclass, field
from math import lcm
from operator import mul
from pathlib import Path

pattern = r"""Monkey (?P<index>\d+):
  Starting items: (?P<items>.*)
  Operation: new = (?P<operation>.*)
  Test: divisible by (?P<divisor>\d+)
    If true: throw to monkey (?P<true_index>\d+)
    If false: throw to monkey (?P<false_index>\d+)"""
re_pattern = re.compile(pattern, re.DOTALL | re.MULTILINE)


def process_input(desc: str) -> dict:
    data = re.match(re_pattern, desc).groupdict()

    data["items"] = list(map(int, data["items"].split(",")))

    int_fields = ["index", "divisor", "true_index", "false_index"]
    for field in int_fields:
        data[field] = int(data[field])

    return data


@dataclass
class Monke:
    items: list[int]

    index: int
    divisor: int
    true_index: int
    false_index: int

    operation: str
    call_count: int = field(default_factory=int)

    def __call__(self, old: int) -> tuple[int, int]:
        self.call_count += 1
        worry = eval(self.operation) % divisors_lcm
        next_monke = self.true_index if worry % self.divisor == 0 else self.false_index
        return worry, next_monke


data_file = Path(__file__).parent / "11.txt"
monkeys = [Monke(**process_input(desc)) for desc in data_file.read_text().split("\n\n")]
divisors_lcm = lcm(*(monkey.divisor for monkey in monkeys))
n_rounds = 10000


for _ in range(n_rounds):
    for monke in monkeys:
        for item in monke.items:
            worry, next_monke = monke(item)
            monkeys[next_monke].items.append(worry)
        monke.items = []

mul(*sorted(monke.call_count for monke in monkeys)[-2:])
