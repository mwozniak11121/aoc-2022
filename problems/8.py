from pathlib import Path

Row = list[int]
Matrix = list[Row]


def parse_input(tree_map: str) -> Matrix:
    return [list(map(int, row)) for row in tree_map.splitlines()]


def create_results(rows: int, cols: int, /, *, fill_value: int) -> Matrix:
    return [[fill_value] * cols for _ in range(rows)]


def rotate_matrix(grid: Matrix) -> Matrix:
    return [
        [grid[j][i] for j in range(len(grid))] for i in range(len(grid[0]) - 1, -1, -1)
    ]


def process_row(row: Row, results_p1: Row, results_p2: Row):
    biggest_tree_so_far = -1
    for i, tree in enumerate(row):
        if tree > biggest_tree_so_far:
            results_p1[i] = True
            biggest_tree_so_far = tree

        results_p2[i] *= next(
            (
                j - i
                for j, next_tree in enumerate(row[i + 1 :], start=i + 1)
                if next_tree >= tree
            ),
            (len(row) - 1) - i,
        )


if __name__ == "__main__":
    data_file = Path(__file__).parent / "8.txt"

    grid = parse_input(data_file.read_text())
    results_p1 = create_results(len(grid), len(grid[0]), fill_value=0)
    results_p2 = create_results(len(grid), len(grid[0]), fill_value=1)

    for _ in range(4):
        for row in range(len(grid)):
            process_row(grid[row], results_p1[row], results_p2[row])

        grid = rotate_matrix(grid)
        results_p1 = rotate_matrix(results_p1)
        results_p2 = rotate_matrix(results_p2)

    sum(sum(results_p1, []))
    max(max(results_p2, key=max))
