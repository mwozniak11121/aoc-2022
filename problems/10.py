from pathlib import Path

data = Path(__file__).parent / "10.txt"
register = 1
result = [0]

for line in data.read_text().splitlines():
    result.append(register)
    match line.split():
        case "noop":
            ...
        case "addx", value:
            result.append(register)
            register += int(value)

sum(result[cycle] * cycle for cycle in range(20, 221, 40))

render = ""
for i, register in enumerate(result[1:], start=1):
    if abs((i - 1) % 40 - register) <= 1:
        render += "#"
    else:
        render += "."

    if i % 40 == 0:
        render += "\n"

print(*render)
