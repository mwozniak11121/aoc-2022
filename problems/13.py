from pathlib import Path


def cmp(a, b) -> bool:
    match a, b:
        case int(), int():
            return None if a == b else a < b
        case int(), list():
            return cmp([a], b)
        case list(), int():
            return cmp(a, [b])
        case list(), list():
            for l, r in zip(a, b):
                if (comparison := cmp(l, r)) is not None:
                    return comparison
            return cmp(len(a), len(b))


data = Path(__file__).parent / "13.txt"

pairs = [
    [eval(x) for x in pair.splitlines()] for pair in data.read_text().split("\n\n")
]
sum(i for i, pair in enumerate(pairs, start=1) if cmp(*pair))

flat = [p for pair in pairs for p in pair]
position_1 = 1 + sum(1 for p in flat if cmp(p, [[2]]))
position_2 = 2 + sum(1 for p in flat if cmp(p, [[6]]))
