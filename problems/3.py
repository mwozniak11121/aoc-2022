from collections.abc import Iterable
from functools import reduce
from pathlib import Path
from string import ascii_letters

data_file = Path(__file__).parent / "3.txt"


def grouper(iterable: Iterable, n: int) -> Iterable:
    return zip(*([iter(iterable)] * n))


def solve_group(group: tuple[str]) -> int:
    elves = (set(elf.strip()) for elf in group)
    common: set = reduce(set.intersection, elves, set(ascii_letters))
    return ascii_letters.index(common.pop()) + 1


if __name__ == "__main__":
    with data_file.open() as f:
        print(sum(solve_group(group) for group in grouper(f, n=3)))
