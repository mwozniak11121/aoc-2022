from collections import defaultdict
from itertools import accumulate
from pathlib import Path


def parse_logs(lines: list[str]) -> dict:
    dir_sizes = defaultdict(int)
    stack = ["/"]
    for line in lines:
        match line.split():
            case "$", "cd", "/":
                stack = ["/"]
            case "$", "cd", "..":
                stack.pop()
            case "$", "cd", dir:
                stack.append(f"{dir}/")
            case "$", "ls":
                ...
            case "dir", _:
                ...
            case size, _:
                for dir in accumulate(stack):
                    dir_sizes[dir] += int(size)

    return dir_sizes


if __name__ == "__main__":
    data_file = Path(__file__).parent / "7.txt"
    dir_sizes = parse_logs(data_file.read_text().split("\n"))
    sum(dir_size for dir_size in dir_sizes.values() if dir_size <= 100000)
    sum_to_free = dir_sizes["/"] - (70000000 - 30000000)
    min(dir_size for dir_size in dir_sizes.values() if dir_size >= sum_to_free)
