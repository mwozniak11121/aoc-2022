from collections.abc import Iterable
from dataclasses import dataclass
from pathlib import Path
from typing import Self


@dataclass
class Point:
    x: int
    y: int

    def __iter__(self) -> Iterable[int]:
        return iter((self.x, self.y))

    def __hash__(self) -> int:
        return hash(tuple(self))

    def __neg__(self) -> Self:
        return Point(*(-v for v in self))

    def __add__(self, other: Self) -> Self:
        return Point(*(v1 + v2 for v1, v2 in zip(self, other)))


def chebyshev(a: Point, b: Point) -> int:
    return max(abs(v1 - v2) for v1, v2 in zip(a, b))


def correction(head: Point, tail: Point) -> Point:
    vector = head + -tail
    return Point((vector.x > 0) - (vector.x < 0), (vector.y > 0) - (vector.y < 0))


if __name__ == "__main__":
    points = [Point(0, 0) for _ in range(10)]
    visited = [{Point(0, 0)} for _ in range(10)]
    values = {"U": (0, 1), "D": (0, -1), "L": (-1, 0), "R": (1, 0)}
    data_file = Path(__file__).parent / "9.txt"

    for instruction in data_file.read_text().splitlines():
        direction, number = instruction.split()
        vector = Point(*values[direction])

        for _ in range(int(number)):
            points[0] += vector

            for i in range(1, 10):
                if chebyshev(points[i], points[i - 1]) > 1:
                    points[i] += correction(points[i - 1], points[i])
                    visited[i].add(points[i])

    print(len(visited[1]))
    print(len(visited[-1]))
