from pathlib import Path

import networkx

data_file = Path(__file__).parent / "12.txt"

points: dict[complex, int] = {}
for y, line in enumerate(data_file.read_text().splitlines()):
    for x, char in enumerate(line):
        points[complex(x, y)] = ord(char)

start = next(point for point, height in points.items() if height == ord("S"))
end = next(point for point, height in points.items() if height == ord("E"))
points[start], points[end] = ord("a"), ord("z")

graph = networkx.DiGraph()
for point, height in points.items():
    for direction in (1, -1, 1j, -1j):
        if height + 1 >= points.get(new_point := point + direction, float("inf")):
            graph.add_edge(point, new_point)

result = networkx.shortest_path_length(graph, target=end)
result.get(start)

starting_points = (point for point, height in points.items() if height == ord("a"))
min(result.get(start, float("inf")) for start in starting_points)
