import re
from itertools import zip_longest
from pathlib import Path
from typing import NamedTuple

test_in = """
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3 """

test_instructions = """move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"""

test_result = "CMZ"


class Instruction(NamedTuple):
    number: int
    from_s: int
    to_s: int


class Grid:
    def __init__(self, data: list[str]):
        self.number_of_crates = self.get_number_of_crates(data[-1])
        self.line_length = self.number_of_crates * 3 + self.number_of_crates - 1
        self.stacks = [list() for _ in range(self.number_of_crates + 1)]
        self.parse_starting_structure(data[:-1])

    def __str__(self) -> str:
        nums = range(1, self.number_of_crates + 1)
        numeration_line = " ".join(f"{num:^3}" for num in nums)

        lines = [numeration_line]
        for chars in zip_longest(*self.stacks[1:], fillvalue=None):
            lines.append(" ".join(f"[{c}]" if c else "   " for c in chars))

        return "\n".join(lines[::-1])

    def __repr__(self):
        return str(self)

    def parse_starting_structure(self, lines: list[str]):
        for line in lines[::-1]:
            lline = line.ljust(self.line_length)
            for stack, char in zip(self.stacks[1:], self._parse_start_line(lline)):
                if char:
                    stack.append(char)

    @staticmethod
    def _parse_start_line(line: str) -> tuple[str]:
        LINE_PATTERN = re.compile(r"([A-Z]\s?|\s{4})")
        return tuple(result.strip() for result in re.findall(LINE_PATTERN, line))

    @staticmethod
    def get_number_of_crates(numeration: str) -> int:
        return max(int(num) for num in re.findall(r"\d", numeration))

    def get_current_result(self) -> str:
        return "".join(stack[-1] for stack in self.stacks[1:])

    def process_instructions(self, instructions: list[str]):
        for instruction in instructions:
            instr = self._translate_instruction(instruction)
            self.stacks[instr.to_s].extend(
                self.stacks[instr.from_s][-instr.number :][::-1]
            )
            del self.stacks[instr.from_s][-instr.number :]

    @staticmethod
    def _translate_instruction(instruction: str) -> Instruction:
        PATTERN = re.compile(
            r"move (?P<number>\d+) from (?P<from_s>\d+) to (?P<to_s>\d+)"
        )
        result = {
            k: int(v) for k, v in re.match(PATTERN, instruction).groupdict().items()
        }
        return Instruction(**result)


def test():
    data = test_in.split("\n")

    grid = Grid(data)
    assert grid.get_current_result() == "NDP"
    assert grid._translate_instruction("move 6 from 4 to 3") == Instruction(
        number=6, from_s=4, to_s=3
    )

    instructions = ["move 1 from 1 to 2"]
    grid.process_instructions(instructions)
    assert grid.get_current_result() == "ZNP"

    grid = Grid(data)
    grid.process_instructions(test_instructions.split("\n"))
    assert grid.get_current_result() == test_result


if __name__ == "__main__":
    data_file = Path(__file__).parent / "5.txt"
    starting_structure, instructions = data_file.read_text().split("\n\n")
    grid = Grid(starting_structure.split("\n"))
    grid.process_instructions(instructions.split("\n"))
    print(grid.get_current_result())
