from pathlib import Path


def find_marker(datastream: str, num_of_distinct: int) -> int | None:
    current_window = set()
    start = 0
    for end, char in enumerate(datastream):
        while char in current_window:
            current_window.remove(datastream[start])
            start += 1

        current_window.add(char)
        if len(current_window) == num_of_distinct:
            return end + 1


if __name__ == "__main__":
    data_file = Path(__file__).parent / "6.txt"

    tests = (
        ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7),
        ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5),
        ("nppdvjthqldpwncqszvftbrmjlhg", 6),
        ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10),
        ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11),
    )
    for datastream, result in tests:
        assert find_marker(datastream, 4) == result

    find_marker(data_file.read_text(), 4)

    tests = (
        ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
        ("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
        ("nppdvjthqldpwncqszvftbrmjlhg", 23),
        ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
        ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26),
    )
    for datastream, result in tests:
        assert find_marker(datastream, 14) == result

    find_marker(data_file.read_text(), 14)
