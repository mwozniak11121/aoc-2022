def function(values: str) -> int:
    elves = [list(map(int, t.split("\n"))) for t in values.strip().split("\n\n")]
    return sum(sorted(sum(elf) for elf in elves)[-3:])
