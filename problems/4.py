from pathlib import Path


def create_range(start: str, stop: str, /) -> set:
    return set(range(int(start), int(stop) + 1))


def solve_single(pairs: str) -> bool:
    left, right = pairs.split(",")
    left_set, right_set = create_range(*left.split("-")), create_range(
        *right.split("-")
    )
    return bool(left_set & right_set)


if __name__ == "__main__":
    data_file = Path(__file__).parent / "4.txt"
    print(sum(solve_single(pairs) for pairs in data_file.read_text().split("\n")))
